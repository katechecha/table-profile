import axios from 'axios'

const defaultParams = {
        headers: {
            'App-Key': 'c26ACvs2Oqva8K9aw2xLx2mU'
        },
        timeout: 10000,
    }

const BASE_URL = 'http://djz.r1i.lan-sevice.com/api';


const api = {
    get: function(urn, options) {
        if (!options) options = {}
        // добавить параметры по умолчанию
        const params = Object.assign(defaultParams, options)
        
        // вернуть промис axios
        return axios.get(BASE_URL + urn, params)
    }
}

export default api;